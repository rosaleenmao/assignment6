/**
 * 
 */

function load(parameter, value) {
	var response;
	//var url = "/assignment6/trafficcamerareports/reports/" + parameter + "/" + value;
	var url = "/trafficcamerareports/reports/" + parameter + "/" + value;
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		response = new XMLHttpRequest();
	} else {// code for IE6, IE5
		response = new ActiveXObject("Microsoft.XMLHTTP");
	}
	response.open("GET", url, false);
	response.send();
	return response.responseText;
}

function myFunction() {
	var local_cs_d = load("camera_status", "DESIRED");
	var local_cs_r = load("camera_status", "REMOVED");
	var local_cs_t = load("camera_status", "TURNED_ON");
	var local_cs_v = load("camera_status", "VOID");
	var local_cm_ad = load("camera_mfg", "Advidia");
	var local_cm_ax = load("camera_mfg", "Axis");
	var local_cm_sa = load("camera_mfg", "Sarix");
	var local_cm_sp = load("camera_mfg", "Spectra%20Enhanced");
	var local_i_no = load("ip_comm_status", "NO%20COMMUNICATION");
	var local_i_off = load("ip_comm_status", "OFFLINE");
	var local_i_on = load("ip_comm_status", "ONLINE");
	tableCreate(local_cs_d, local_cs_r, local_cs_t, local_cs_v,
			local_cm_ad, local_cm_ax, local_cm_sa, local_cm_sp,
			local_i_no, local_i_off, local_i_on);
}

function tableCreate(local_cs_d, local_cs_r, local_cs_t, local_cs_v,
		local_cm_ad, local_cm_ax, local_cm_sa, local_cm_sp,
		local_i_no, local_i_off, local_i_on) {
	document.body.innerHTML = '';
	var body = document.body, tbl = document.createElement('table');
	tbl.style.width = '600px';
	tbl.style.border = '1px solid black';

	//first row
	var tr = tbl.insertRow();

	//first column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Camera Attribute'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';

	//second column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Camera Attribute Value'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';

	//third column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Count'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';
	
	//row 2
	var csrow1 = tbl.insertRow();
	
	var csattribute1 = csrow1.insertCell();
	csattribute1.appendChild(document.createTextNode('camera_status'));
	var csvalue1 = csrow1.insertCell();
	csvalue1.appendChild(document.createTextNode('DESIRED'));
	var cscount1 = csrow1.insertCell();
	cscount1.appendChild(document.createTextNode(local_cs_d));
	
	//row 3
	var csrow2 = tbl.insertRow();
	
	var csattribute2 = csrow2.insertCell();
	csattribute2.appendChild(document.createTextNode('camera_status'));
	var csvalue2 = csrow2.insertCell();
	csvalue2.appendChild(document.createTextNode('REMOVED'));
	var cscount2 = csrow2.insertCell();
	cscount2.appendChild(document.createTextNode(local_cs_r));

	//row 4
	var csrow3 = tbl.insertRow();
	
	var csattribute3 = csrow3.insertCell();
	csattribute3.appendChild(document.createTextNode('camera_status'));
	var csvalue3 = csrow3.insertCell();
	csvalue3.appendChild(document.createTextNode('TURNED_ON'));
	var cscount3 = csrow3.insertCell();
	cscount3.appendChild(document.createTextNode(local_cs_t));
	
	//row 5
	var csrow4 = tbl.insertRow();
	
	var csattribute4 = csrow4.insertCell();
	csattribute4.appendChild(document.createTextNode('camera_status'));
	var csvalue4 = csrow4.insertCell();
	csvalue4.appendChild(document.createTextNode('VOID'));
	var cscount4 = csrow4.insertCell();
	cscount4.appendChild(document.createTextNode(local_cs_v));
	
	//row 6
	var csrow5 = tbl.insertRow();
	
	var csattribute5 = csrow5.insertCell();
	csattribute5.appendChild(document.createTextNode('camera_mfg'));
	var csvalue5 = csrow5.insertCell();
	csvalue5.appendChild(document.createTextNode('Advidia'));
	var cscount5 = csrow5.insertCell();
	cscount5.appendChild(document.createTextNode(local_cm_ad));

	//row 7
	var csrow6 = tbl.insertRow();
	
	var csattribute6 = csrow6.insertCell();
	csattribute6.appendChild(document.createTextNode('camera_mfg'));
	var csvalue6 = csrow6.insertCell();
	csvalue6.appendChild(document.createTextNode('Axis'));
	var cscount6 = csrow6.insertCell();
	cscount6.appendChild(document.createTextNode(local_cm_ax));

	//row 8
	var csrow7 = tbl.insertRow();
	
	var csattribute7 = csrow7.insertCell();
	csattribute7.appendChild(document.createTextNode('camera_mfg'));
	var csvalue7 = csrow7.insertCell();
	csvalue7.appendChild(document.createTextNode('Sarix'));
	var cscount7 = csrow7.insertCell();
	cscount7.appendChild(document.createTextNode(local_cm_sa));
	
	//row 9
	var csrow8 = tbl.insertRow();
	
	var csattribute8 = csrow8.insertCell();
	csattribute8.appendChild(document.createTextNode('camera_mfg'));
	var csvalue8 = csrow8.insertCell();
	csvalue8.appendChild(document.createTextNode('Spectra'));
	var cscount8 = csrow8.insertCell();
	cscount8.appendChild(document.createTextNode(local_cm_sp));

	//row 10
	var csrow9 = tbl.insertRow();
	
	var csattribute9 = csrow9.insertCell();
	csattribute9.appendChild(document.createTextNode('ip_comm_status'));
	var csvalue9 = csrow9.insertCell();
	csvalue9.appendChild(document.createTextNode('NO COMMUNICATION'));
	var cscount9 = csrow9.insertCell();
	cscount9.appendChild(document.createTextNode(local_i_no));

	//row 11
	var csrow10 = tbl.insertRow();
	
	var csattribute10 = csrow10.insertCell();
	csattribute10.appendChild(document.createTextNode('ip_comm_status'));
	var csvalue10 = csrow10.insertCell();
	csvalue10.appendChild(document.createTextNode('OFFLINE'));
	var cscount10 = csrow10.insertCell();
	cscount10.appendChild(document.createTextNode(local_i_off));

	//row 12
	var csrow11 = tbl.insertRow();
	
	var csattribute11 = csrow11.insertCell();
	csattribute11.appendChild(document.createTextNode('ip_comm_status'));
	var csvalue11 = csrow11.insertCell();
	csvalue11.appendChild(document.createTextNode('ONLINE'));
	var cscount11 = csrow11.insertCell();
	cscount11.appendChild(document.createTextNode(local_i_on));

	body.appendChild(tbl);
}