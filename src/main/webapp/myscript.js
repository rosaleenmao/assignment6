/**
 * 
 */
//camera status
var cs_d;
var cs_r;
var cs_t;
var cs_v;

//camera mfg
var cm_ad;
var cm_ax;
var cm_sa;
var cm_sp;

//ip comm status
var i_no;
var i_off;
var i_on;

function load_cs_d() {
	var url = "/assignment6/trafficcamerareports/reports/camera_status/DESIRED";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cs_d = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cs_d = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cs_d.onreadystatechange = function() {
		load_cs_r();
	};
	cs_d.open("GET", url, false);
	cs_d.send();
}

function load_cs_r() {
	var url = "/assignment6/trafficcamerareports/reports/camera_status/REMOVED";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cs_r = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cs_r = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cs_r.onreadystatechange = function() {
		load_cs_t()
	};
	cs_r.open("GET", url, false);
	cs_r.send();
}

function load_cs_t() {
	var url = "/assignment6/trafficcamerareports/reports/camera_status/TURNED_ON";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cs_t = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cs_t = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cs_t.onreadystatechange = function() {
		load_cs_v();
	};
	cs_t.open("GET", url, false);
	cs_t.send();
}

function load_cs_v() {
	var url = "/assignment6/trafficcamerareports/reports/camera_status/VOID";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cs_v = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cs_v = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cs_v.onreadystatechange = function() {
		load_cm_ad();
	};
	cs_v.open("GET", url, false);
	cs_v.send();
}

function load_cm_ad() {
	var url = "/assignment6/trafficcamerareports/reports/camera_mfg/Advidia"
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cm_ad = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cm_ad = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cm_ad.onreadystatechange = function() {
		load_cm_ax();
	};
	cm_ad.open("GET", url, false);
	cm_ad.send();
}

function load_cm_ax() {
	var url = "/assignment6/trafficcamerareports/reports/camera_mfg/Axis"
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cm_ax = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cm_ax = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cm_ax.onreadystatechange = function() {
		load_cm_sa();
	};
	cm_ax.open("GET", url, false);
	cm_ax.send();
}

function load_cm_sa() {
	var url = "/assignment6/trafficcamerareports/reports/camera_mfg/Sarix"
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cm_sa = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cm_sa = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cm_sa.onreadystatechange = function() {
		load_cm_sp();
	};
	cm_sa.open("GET", url, false);
	cm_sa.send();
}

function load_cm_sp() {
	var url = "/assignment6/trafficcamerareports/reports/camera_mfg/Spectra%20Enhanced"
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		cm_sp = new XMLHttpRequest();
	} else {// code for IE6, IE5
		cm_sp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	cm_sp.onreadystatechange = function() {
		load_i_no();
	};
	cm_sp.open("GET", url, false);
	cm_sp.send();
}

function load_i_no() {
	var url = "/assignment6/trafficcamerareports/reports/ip_comm_status/NO%20COMMUNICATION";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		i_no = new XMLHttpRequest();
	} else {// code for IE6, IE5
		i_no = new ActiveXObject("Microsoft.XMLHTTP");
	}
	i_no.onreadystatechange = function() {
		load_i_off();
	};
	i_no.open("GET", url, false);
	i_no.send();
}

function load_i_off() {
	var url = "/assignment6/trafficcamerareports/reports/ip_comm_status/OFFLINE";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		i_off = new XMLHttpRequest();
	} else {// code for IE6, IE5
		i_off = new ActiveXObject("Microsoft.XMLHTTP");
	}
	i_off.onreadystatechange = function() {
		load_i_on();
	};
	i_off.open("GET", url, false);
	i_off.send();
}

function load_i_on() {
	var url = "/assignment6/trafficcamerareports/reports/ip_comm_status/ONLINE";
	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		i_on = new XMLHttpRequest();
	} else {// code for IE6, IE5
		i_on = new ActiveXObject("Microsoft.XMLHTTP");
	}
	i_on.onreadystatechange = function() {
		tableCreate(cs_d.responseText, cs_r.responseText, cs_t.responseText,
				cs_v.responseText, cm_ad.responseText, cm_ax.responseText,
				cm_sa.responseText, cm_sp.responseText, i_no.responseText, i_off.responseText, i_on.responseText);
	};
	i_on.open("GET", url, false);
	i_on.send();
}

function loadXMLDocs() {

	load_cs_d();
}

function myFunction() {
	loadXMLDocs();
}

function tableCreate(cs_d_text, cs_r_text, cs_t_text, cs_v_text, cm_ad_text,
		cm_ax_text, cm_sa_text, cm_sp_text, i_no_text, i_off_text, i_on_text) {
	document.body.innerHTML = '';
	var body = document.body, tbl = document.createElement('table');
	tbl.style.width = '600px';
	tbl.style.border = '1px solid black';

	//first row
	var tr = tbl.insertRow();

	//first column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Camera Attribute'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';

	//second column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Camera Attribute Value'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';

	//third column header
	var td = tr.insertCell();
	td.appendChild(document.createTextNode('Count'));
	td.style.border = '1px solid black';
	td.style.fontWeight = 'bold';
	

	for (var i = 0; i < 4; i++) {
		var csrow = tbl.insertRow();
		var csattribute = csrow.insertCell();
		csattribute.appendChild(document.createTextNode('camera_status'));
		switch (i) {
		case 0:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('DESIRED'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cs_d_text));
			break;
		case 1:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('REMOVED'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cs_r_text));
			break;
		case 2:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('TURNED_ON'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cs_t_text));
			break;
		case 3:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('VOID'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cs_v_text));
			break;
		default:
		}
	}

	for (var i = 0; i < 4; i++) {
		var csrow = tbl.insertRow();
		var csattribute = csrow.insertCell();
		csattribute.appendChild(document.createTextNode('camera_mfg'));
		switch (i) {
		case 0:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('Advidia'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cm_ad_text));
			break;
		case 1:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('Axis'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cm_ax_text));
			break;
		case 2:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('Sarix'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cm_sa_text));
			break;
		case 3:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('Spectra'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(cm_sp_text));
			break;
		default:
		}
	}
	
	for (var i = 0; i < 3; i++) {
		var csrow = tbl.insertRow();
		var csattribute = csrow.insertCell();
		csattribute.appendChild(document.createTextNode('ip_comm_status'));
		switch (i) {
		case 0:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('NO COMMUNICATION'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(i_no_text));
			break;
		case 1:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('OFFLINE'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(i_off_text));
			break;
		case 2:
			var csvalue = csrow.insertCell();
			csvalue.appendChild(document.createTextNode('ONLINE'));
			var cscount = csrow.insertCell();
			cscount.appendChild(document.createTextNode(i_on_text));
			break;
		default:
		}
	}

	body.appendChild(tbl);
}