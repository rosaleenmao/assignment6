package assign.services;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class TrafficCameraReportsService {
	
	public int findCameras(String parameter, String value) throws JAXBException, MalformedURLException, IOException
	{
		int numberOfCameras = 0;
	    JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	     
	    Response cameraNodes = (Response) jaxbUnmarshaller.unmarshal( 
	    		new URL("http://www.cs.utexas.edu/~devdatta/traffic_camera_data.xml").openStream());
	    if (parameter.equals("camera_mfg")) {
	    	for(Row camera : cameraNodes.getRowList()) {
		        if(camera.getCameraMfg() != null && camera.getCameraMfg().equalsIgnoreCase(value)) numberOfCameras++;
		    }
	    } else if (parameter.equals("camera_status")) {
	    	for(Row camera : cameraNodes.getRowList()) {
		        if(camera.getCameraStatus() != null && camera.getCameraStatus().equalsIgnoreCase(value)) numberOfCameras++;
		    }
	    } else if (parameter.equals("ip_comm_status")) {
	    	for(Row camera : cameraNodes.getRowList()) {
	    		if(camera.getIpCommStatus() != null && camera.getIpCommStatus().equalsIgnoreCase(value)) numberOfCameras++;
		    }
	    }
	    return numberOfCameras;
	}
}
