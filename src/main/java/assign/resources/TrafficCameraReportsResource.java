package assign.resources;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.services.TrafficCameraReportsService;

@Path("/")
public class TrafficCameraReportsResource {
	
TrafficCameraReportsService trafficCameraReportsService;
	
	public TrafficCameraReportsResource() {
		// Dependency Inject not used.
		trafficCameraReportsService = new TrafficCameraReportsService();
	}
	
	public void setTrafficCameraReportsService(TrafficCameraReportsService service) {
		trafficCameraReportsService = service;
	}
	
	@GET
	@Path("/dog")
	@Produces("text/html")
	public String welcome() {
		return "This works";
	}	

	@GET
	@Path("/reports/{parameter}/{value}")
	@Produces("text/html")
	public String getReport(@PathParam("parameter") String parameter, @PathParam("value") String value) throws Exception {
		int result = trafficCameraReportsService.findCameras(parameter, value);
		return "" + result;
	}		
}